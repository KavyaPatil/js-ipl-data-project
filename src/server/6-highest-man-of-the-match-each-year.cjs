const fs = require("fs");
const csvtojson = require("csvtojson");
const csvFilePath = "../data/matches.csv";
const jsonFilePath = "../public/6-highest-man-of-the-match-each-year.json";



async function manOfTheMatchFunction(csvFilePath) {

  const data = await csvtojson().fromFile(csvFilePath);

  const manOfTheMatch = data.reduce((newObj, element) => {
    if (element.season in newObj) {
      if (element.player_of_match in newObj[element.season]) {
        newObj[element.season][element.player_of_match] += 1;

      } else {
        newObj[element.season][element.player_of_match] = 1;

      }
    } else {
      newObj[element.season] = {};
      newObj[element.season][element.player_of_match] = 1;

    }

    return newObj;
  }, {});

 
  const arrayOfManOfTheMatch = Object.keys(manOfTheMatch).map((season) => {
   const arrayOfPlayers =  Object.entries(manOfTheMatch[season]).sort((player1, player2) => {

      return player2[1] - player1[1];

    });

    return arrayOfPlayers[0];

  });


  return JSON.stringify(Object.fromEntries(arrayOfManOfTheMatch), null, 2);
}



manOfTheMatchFunction(csvFilePath).then((data) => {

  fs.writeFile(jsonFilePath, data, "utf8", (err) => {

    if (err) {
      console.error("An error occurred ", err);

    } else {
      console.log("success");
    }
  });
});
