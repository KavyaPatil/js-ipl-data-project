const fs = require("fs");
const csvtojson = require("csvtojson");
const csvMatchFilePath = "../data/matches.csv";
const csvDeliveriesFilePath = "../data/deliveries.csv";
const jsonFilePath = "../public/7-strike-rate-of-batsman-for-each-season.json";

async function strikeRateFunction() {
  const matcheData = await csvtojson().fromFile(csvMatchFilePath);
  const deliveriesData = await csvtojson().fromFile(csvDeliveriesFilePath);

  const strikeBallsruns = deliveriesData.reduce((newObj, element) => {
    const yearStrikeRate = matcheData.find((item) => {
      return item.id === element.match_id;
    });

    const year = yearStrikeRate.season;

    if (element.batsman === "MS Dhoni" && element.batsman in newObj) {
      if (year in newObj[element.batsman]) {
        newObj[element.batsman][year].balls += 1;
        newObj[element.batsman][year].runs += +element.batsman_runs;
      } else {
        newObj[element.batsman][year] = {};
        newObj[element.batsman][year].balls = 1;
        newObj[element.batsman][year].runs = +element.batsman_runs;
      }
    } else if (
      element.batsman === "MS Dhoni" &&
      element.batsman in newObj == false
    ) {
      newObj[element.batsman] = {};
      newObj[element.batsman][year] = {};
      newObj[element.batsman][year].balls = 1;
      newObj[element.batsman][year].runs = +element.batsman_runs;
    }

    return newObj;
  }, {});

  const strikeRate = Object.entries(strikeBallsruns).reduce((newObj, element) => {

      const arrayOfObjects = Object.keys(element[1]).map((season) => {
        const obj = {};

        obj[season] = Math.floor((element[1][season].runs / element[1][season].balls) * 100);
        console.log(obj);

        return obj;

      });

      newObj[element[0]] = arrayOfObjects;

      return newObj;
    },
    {}
  );

  // console.log(strikeRate);
  return JSON.stringify(strikeRate, null, 2);
}

strikeRateFunction().then((data) => {

  fs.writeFile(jsonFilePath, data, "utf8", (err) => {

    if (err) {
      console.error("An error occurred ", err);
      
    } else {
      console.log("success");
    }
  });
});
