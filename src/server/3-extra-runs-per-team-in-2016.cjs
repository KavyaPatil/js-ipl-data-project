const fs = require("fs");
const csvtojson = require("csvtojson");
const csvMatchFilePath = "../data/matches.csv";
const csvDeliveriesFilePath = "../data/deliveries.csv";
const jsonFilePath = "../public/3-extra-runs-per-team-in-2016.json";

async function extraRunsConcededFunction(csvMatchFilePath) {

  const matcheData = await csvtojson().fromFile(csvMatchFilePath);
  const deliveriesData = await csvtojson().fromFile(csvDeliveriesFilePath);

  const data_2016 = matcheData
    .filter((item) => {
      return item.season === "2016";
    })
    .map((ele) => {
      return ele.id;
    });


  let extraRuns = deliveriesData.reduce((newObj, item) => {

    if (data_2016.includes(item.match_id)) {

      if (item.bowling_team in newObj) {
        newObj[item.bowling_team] += +item.extra_runs;

      } else {
        newObj[item.bowling_team] = +item.extra_runs;

      }
    }
    return newObj;
    
  }, {});

  console.log(extraRuns);

  return JSON.stringify(extraRuns, null, 2);

}

extraRunsConcededFunction(csvMatchFilePath).then((data) => {

  fs.writeFile(jsonFilePath, data, "utf8", (err) => {
     
    if (err) {
      console.error("An error occurred ", err);
      
    } else {
      console.log("success");
    }
  });
});
