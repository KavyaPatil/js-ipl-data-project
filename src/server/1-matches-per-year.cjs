const fs = require("fs");
const csvtojson = require("csvtojson");
const csvFilePath = "../data/matches.csv";
const jsonFilePath = "../public/1-matches-per-year.json";



async function matchesPerYearFunction(csvFilePath) {

  const data = await csvtojson().fromFile(csvFilePath);

  const matchesPerYear = data.reduce((newObj, key) => {

    if (key.season in newObj) {

      newObj[key.season] += 1;

    } else {

      newObj[key.season] = 1;
    }

    return newObj;

  }, {});      //initialising new object and storing the data in that object using reduce

  return JSON.stringify(matchesPerYear, null, 2);
}

matchesPerYearFunction(csvFilePath).then((data) => {

  fs.writeFile(jsonFilePath, data, "utf8", (err) => {

    if (err) {
      console.error("An error occurred ", err);

    } else {
      console.log("success");
    }
  });
});
