const fs = require("fs");
const csvtojson = require("csvtojson");
const csvFilePath = "../data/matches.csv";
const jsonFilePath = "../public/2-matches-won-per-team-per-year.json";



async function matchesWonPerTeamFunction(csvFilePath) {

  const data = await csvtojson().fromFile(csvFilePath);

  const matchesWon = data.reduce((newObj, ele) => {

    if (ele.season in newObj) {

      if (ele.winner in newObj[ele.season]) {
        newObj[ele.season][ele.winner] += 1;

      } else {
        newObj[ele.season][ele.winner] = 1;

      }
    } else {
      newObj[ele.season] = {};
      newObj[ele.season][ele.winner] = 1;

    }

    return newObj;

  }, {});

  console.log(matchesWon);

  return JSON.stringify(matchesWon, null, 2);
}

matchesWonPerTeamFunction(csvFilePath).then((data) => {

  fs.writeFile(jsonFilePath, data, "utf8", (err) => {

    if (err) {
      console.error("An error occurred ", err);

    } else {
      console.log("success");
    }
  });
});
