const fs = require("fs");
const csvtojson = require("csvtojson");
const csvMatchFilePath = "../data/matches.csv";
const csvDeliveriesFilePath = "../data/deliveries.csv";
const jsonFilePath =
  "../public/5-number-of-times-each-team-won-toss-won-match.json";



async function teamWonTossWonMatchFunction(csvMatchFilePath) {
  
  const matcheData = await csvtojson().fromFile(csvMatchFilePath);

  const teamWonTossWonMatch = matcheData.reduce((newObj, element) => {

    if (element.toss_winner === element.winner) {

      if (element.winner in newObj) {
        newObj[element.winner] += 1;

      } else {
        newObj[element.winner] = 1;
      }
    }

    return newObj;

  }, {});

  console.log(teamWonTossWonMatch);

  return JSON.stringify(teamWonTossWonMatch);
}



teamWonTossWonMatchFunction(csvMatchFilePath).then((data) => {

  fs.writeFile(jsonFilePath, data, "utf8", (err) => {

    if (err) {
      console.error("An error occurred ", err);

    } else {
      console.log("success");
    }
  });
});
