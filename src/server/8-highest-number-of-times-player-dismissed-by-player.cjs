const fs = require("fs");
const csvtojson = require("csvtojson");
const csvFilePath = "../data/deliveries.csv";
const jsonFilePath =
  "../public/8-highest-number-of-times-player-dismissed-by-player.json";

async function playerDismissedFunction(csvFilePath) {

  const data = await csvtojson().fromFile(csvFilePath);

  const dismissedPlayer = data
    .filter((item) => {
      return item.player_dismissed !== "";
      
    })
    .reduce((newObj, element) => {
      if (element.batsman in newObj) {
        if (element.bowler in newObj[element.batsman]) {
          newObj[element.batsman][element.bowler] += 1;

        } else {
          newObj[element.batsman][element.bowler] = 1;

        }
      } else {
        newObj[element.batsman] = {};
        newObj[element.batsman][element.bowler] = 1;

      }
      return newObj;
    }, {});

  // console.log(dismissedPlayer);

  const arrayOfObjects = Object.keys(dismissedPlayer).map((player) => {
    const object = {};

    const arrayOfPlayers = Object.entries(dismissedPlayer[player]).sort( (bowler1, bowler2) => {
        return bowler2[1] - bowler1[1];

      }
    );

    object[player] = arrayOfPlayers[0];

    return object;
  });
  

  let max = 0;
  const result = arrayOfObjects.reduce((newObj, element) => {
    let key = Object.keys(element);

    if (element[key][1] > max) {
      max = element[key][1];
      newObj = { ...element };

    } else {
      newObj = { ...newObj };
    }

    return newObj;
  }, {});



  return JSON.stringify(result);
}

playerDismissedFunction(csvFilePath).then((data) => {

  fs.writeFile(jsonFilePath, data, "utf8", (err) => {

    if (err) {
      console.error("An error occurred ", err);

    } else {
      console.log("success");
    }
  });
});
