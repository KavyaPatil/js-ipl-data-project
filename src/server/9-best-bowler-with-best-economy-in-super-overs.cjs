const fs = require("fs");
const csvtojson = require("csvtojson");
const csvFilePath = "../data/deliveries.csv";
const jsonFilePath =
  "../public/9-best-bowler-with-best-economy-in-super-overs.json";

async function topEconomicBowlerFunction(csvFilePath) {
  const jsonArray = await csvtojson().fromFile(csvFilePath);

  let data = jsonArray.filter((item) => {
    return item.is_super_over !== "0";

  });

  const bowlers = data.reduce((newObj, element) => {

    if (element.bowler in newObj) {
      newObj[element.bowler] += +element.total_runs;

    } else {
      newObj[element.bowler] = +element.total_runs;

    }

    return newObj;

  }, {});

  let min = Infinity;
  let topEconomicBowler;

  const bestEconomy = Object.keys(bowlers).reduce((newObj, bowler) => {

    if (bowlers[bowler] < min) {
      min = bowlers[bowler];
      topEconomicBowler = bowler;
       
      newObj.bowler = topEconomicBowler;
      newObj.economy = min;

    } else {
      newObj = { ...newObj };

    }

    return newObj;

  }, {});


  return JSON.stringify(bestEconomy);
}

topEconomicBowlerFunction(csvFilePath).then((data) => {

  fs.writeFile(jsonFilePath, data, "utf8", (err) => {

    if (err) {
      console.error("An error occurred ", err);

    } else {
      console.log("success");
    }
  });
});
