const fs = require("fs");
const csvtojson = require("csvtojson");
const csvFilePath = "../data/deliveries.csv";
const csvDeliveriesFilePath = "../data/matches.csv";
const jsonFilePath = "../public/4-top-ten-economic-bowlers-2015.json";

async function topTenEconomicBowlerFunction(csvFilePath) {

  const deliveriesData = await csvtojson().fromFile(csvFilePath);
  const matchData = await csvtojson().fromFile(csvDeliveriesFilePath);

  let data_2015 = matchData
    .filter((item) => {
      return item.season === "2015";
    })
    .map((element) => {
      return element.id;
    });

  const economicBowlers = deliveriesData.reduce((newObj, item) => {

    if (data_2015.includes(item.match_id)) {
       
      if (item.bowler in newObj) {
      
        if (+item.ball > 6) {
          newObj[item.bowler].runs += +item.total_runs;

        } else {
          newObj[item.bowler].balls += 1;
          newObj[item.bowler].runs += +item.total_runs;

        }
      } else {

        if (item.extra_runs !== "0") {
          newObj[item.bowler] = {};
          newObj[item.bowler].balls = 0;
          newObj[item.bowler].runs = +item.total_runs;

        } else {
          newObj[item.bowler] = {};
          newObj[item.bowler].balls = 1;
          newObj[item.bowler].runs = +item.total_runs;
        }
      }
    }

    return newObj;
  }, {});

  // console.log(economicBowlers);

  let entries = Object.entries(economicBowlers).sort((bowler1, bowler2) => {
      const bowlerEconomy1 = Math.floor((bowler1[1].runs / bowler1[1].balls) * 6);
      const bowlerEconomy2 = Math.floor((bowler2[1].runs / bowler2[1].balls) * 6);
      
      return bowlerEconomy1 - bowlerEconomy2;

    })
    .slice(0, 10);

  // console.log(entries);

  const topTenEconomicBowlers = Object.fromEntries(entries);
  return JSON.stringify(topTenEconomicBowlers);
}



topTenEconomicBowlerFunction(csvFilePath).then((data) => {

  fs.writeFile(jsonFilePath, data, "utf8", (err) => {

    if (err) {
      console.error("An error occurred ", err);

    } else {
      console.log("success");
    }
  });
});
